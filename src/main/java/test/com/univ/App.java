package test.com.univ;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.EntityTransaction;
import javax.persistence.Persistence;

import com.univ.Person;
import com.univ.Rent;

/**
 * Hello world!
 *
 */
public class App 
{
    public static void main( String[] args )
    {
    	EntityManagerFactory emf = Persistence.createEntityManagerFactory("manager1");
		EntityManager entityManager = emf.createEntityManager();
		
		EntityTransaction tx = entityManager.getTransaction();
		
    	try{
    		
			tx.begin();
			
			Person p = new Person();
			p.setName("Tintin");
					
			DateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy");
			Date beginDate = dateFormat.parse("23/09/2015");
			p.setDate(beginDate);
			Rent r=new Rent();
			r.setPerson(p);
			r.setBeginRent(dateFormat.parse("23/09/2015"));
			r.setEndRent(dateFormat.parse("26/09/2015"));
			p.getRents().add(r);
			entityManager.persist(r);
				
			tx.commit();			
		
		}catch(Exception e){
			tx.rollback();
		}
		
	}
}
