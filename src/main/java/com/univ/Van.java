package com.univ;

import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;

@Entity
@DiscriminatorValue("VAN")
public class Van extends Vehicule {
	
	private long maxWeight;

	public long getMaxWeight() {
		return maxWeight;
	}

	public void setMaxWeight(long maxWeight) {
		this.maxWeight = maxWeight;
	}
	
	
}
