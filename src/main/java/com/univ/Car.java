package com.univ;

import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;
import javax.persistence.Inheritance;
import javax.persistence.InheritanceType;

@Entity
@DiscriminatorValue("CAR")
public class Car extends Vehicule{
	
private int numberOfSeats;

public int getNumberOfSeats() {
	return numberOfSeats;
}

public void setNumberOfSeats(int numberOfSeats) {
	this.numberOfSeats = numberOfSeats;
}


}
